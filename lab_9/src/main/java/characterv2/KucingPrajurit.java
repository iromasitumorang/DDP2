package characterv2;
import java.math.BigInteger;

public class KucingPrajurit extends KucingRakyat {
	BigInteger kali2=new BigInteger ("2");
    public KucingPrajurit (String name, String health, String power){
		super(name,health,power);
    }

    public void attack(Kucing kucing){
		if(health.compareTo(zeroLife)==1){
			if(kucing.health.compareTo(getPower().multiply(kali2))==-1){
				kucing.health=super.zeroLife;
			}
			else{
				kucing.health=kucing.health.subtract(getPower().multiply(kali2));
			}
		}
	}
}
