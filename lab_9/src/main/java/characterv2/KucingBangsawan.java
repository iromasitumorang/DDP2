package characterv2;
import java.math.BigInteger;

public class KucingBangsawan extends Kucing{
	String title;

    public KucingBangsawan(String name, String health, String power, String title){
		super(name,health,power);
		this.title=title;
    }

    public String getTitle() {
		return title;
    }

    public void setTitle(String title) {
		this.title=title;
    }

    public void execute(KucingRakyat kucing){
		if(health.compareTo(zeroLife)==1)kucing.health=super.zeroLife;
	}

    public String status(){
		return "Nama\t: "+getName()+"\nHealth\t: "+getHealth()+"\nPower\t: "+getPower()+"\nTitle\t: "+getTitle();

    }

}
