package characterv2;
import java.math.BigInteger;

public class KucingRakyat extends Kucing {
BigInteger tax=new BigInteger("200000000000000000000000000000000000");

    public KucingRakyat(String name, String health, String power){
		super(name,health,power);
    }

    public void payTax(){
		if(health.compareTo(zeroLife)==1)health=health.subtract(tax);
	}
}
