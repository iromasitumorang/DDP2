package characterv2;
import java.math.BigInteger;

public class Kucing {
	String name;
	BigInteger health,power;
	BigInteger zeroLife=new BigInteger ("0");
	BigInteger sleep=new BigInteger ("200000000000000000000000000000000000");
    public Kucing(String name, String health, String power){
		this.name=name;
		this.health=new BigInteger (health);
		this.power=new BigInteger (power);
    }

    public String getName() {
		return name;
    }

    public void setName(String name) {
		this.name=name;
	}

    public BigInteger getHealth() {
		return health;
    }

    public void setHealth(BigInteger health) {
		this.health=health;
	}

    public BigInteger getPower() {
		return power;
    }

    public void setPower(BigInteger power) {
		this.power=power;
	}

    public void attack(Kucing kucing) {
		if(health.compareTo(zeroLife)==1){
			if(kucing.health.compareTo(power)==1){
kucing.health=kucing.health.subtract(getPower());
}
			else{
kucing.health=zeroLife;
}
		}
	}

    public void sleep() {
		if (health.compareTo(zeroLife)==1){
			health=health.add(sleep);
		}else{
			health=zeroLife;
		}
	}
	
	public void membandingkan (Kucing kitty){
	if(kitty.health.compareTo(this.health)==-1){}
		else if(kitty.health.compareTo(this.health)==1){}
			else{if(kitty.power.compareTo(this.power)==-1){}
				else if(kitty.power.compareTo(this.power)==1){}
			else{}}
	}

    public String status(){
		return "Nama\t: "+getName()+"\nHealth\t: "+getHealth()+"\nPower\t: "+getPower();
    }
}
