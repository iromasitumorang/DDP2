/**
 * Tutorial Lab 4 DDP2
 * Member Pet Shop
 */

/**
 * @author Iroma
 */
public class Member {
    /**
     * Class untuk maintain data pembeli
     */
    public  int nomorMember =0,umurMember=0,saldo=0;
    private String nama;
    private boolean statusMember=false;

    public Member(int nomorMember, String nama,int umurMember, int saldo){
        this.nomorMember=nomorMember;
        this.nama=nama;
        this.umurMember=umurMember;
        this.saldo=saldo;
    }
    public Member(int nomorMember, String nama,int umurMember){
        this.nomorMember=nomorMember;
        this.nama=nama;
        this.umurMember=umurMember;
        this.saldo=50000;
    }
    /**
     *
     * @param umurMember
     */
    public void setUmur(int umurMember){
        this.umurMember= umurMember;
        if (umurMember == 60){
            System.out.println(this.getNama()+" terlalu tua untuk menjadi member");
            this.tutupMember();
        }
    }
    public int getUmur() {
        return this.umurMember;
    }
    public void setNama(String nama){
        this.nama=nama;
    }
    public String getNama()
    {
        return this.nama;
    }
    public void setNomorMember(int nomorMember){
        this.nomorMember=nomorMember;
    }
    public int getNomorMember(){
        return this.nomorMember;
    }
    public void setStatusMember(boolean statusMember){
        this.statusMember=statusMember;
    }
    public boolean isMemberTutup(){
        return this.statusMember;
    }
    public void setSaldo(int saldo){
        this.saldo= saldo;
    }
    public int getSaldo(){
        return this.saldo;
    }

    public void kirimSaldo(Member B, int uang){
        /**
         * Fungsi melakukan transfer saldo sejumlah inputan kepada penerima
         */
        int saldo=0;
        if (this.getNama().equals(B.getNama())){
            System.out.println("Rekening Penerima tidak boleh sama dengan rekening pemberi");
        }
        else if(isMemberTutup()==true || this.isMemberTutup()==true)
        {
            System.out.println("Pengiriman saldo gagal. Rekening penerima atau pemberi sudah ditutup");
        }
        else{
            if (this.saldo<uang){
                System.out.println("Saldo tidak mencukupi");
            }
            else {
                this.saldo = this.saldo-uang;
                B.saldo +=uang;
                System.out.println(this.getNama()+" telah berhasil mengirimkan saldo ke "+B.getNama()+" sebesar "+uang);
                System.out.println("Saldo "+this.getNama()+" saat ini "+this.getSaldo());
                System.out.println("Saldo "+B.getNama()+" saat ini "+B.getSaldo());
            }
        }
    }
    public int topUpSaldo(int uang){
        if (isMemberTutup()==true){
            System.out.println("Member atas nama "+getNama()+" telah ditutup");
        }
        else {
            this.saldo +=uang;
            System.out.println(getNama()+" telah berhasil top up saldo");
            System.out.println("Saldo "+getNama()+ " saat ini "+getSaldo());
        }
        return this.saldo;
    }

    public void tutupMember(){
        this.setStatusMember(true);
        System.out.println("Member atas nama "+this.getNama()+ " telah resmi ditutup \n");
    }
    public String toString(){
        String hasil="";
        if(isMemberTutup()==true){
            System.out.println("Member atas nama "+getNama()+" telah ditutup");
        }
        else {
            hasil = "Nomor Member : " + this.getNomorMember() + "\n" +
                    "           Nama         : "+ this.getNama() +"\n" +
                    "           Umur         : " + this.getUmur() +"\n" +
                    "           Saldo        : " + this.getSaldo();
        }
        return hasil;
    }

}
