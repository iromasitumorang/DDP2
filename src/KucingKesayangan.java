public class KucingKesayangan {
    String namaKucing, rasKucing;
    int umurKucing;

    public void setUmurKucing(int umurKucing){
        this.umurKucing=umurKucing;
    }
    public void setNamaKucing(String namaKucing){
        this.namaKucing=namaKucing;
    }
    public String getNamaKucing(){
        return namaKucing;
    }
    public void setRasKucing(String rasKucing){
        this.rasKucing=rasKucing;
    }
    public void mengeong(){
        System.out.println("Meooooooonnnnnng");
    }
    public int berjalan(int jlhLangkah){
        String nama = getNamaKucing();
        System.out.println("Kucingku, "+nama+" telah berjalan sejauh "+jlhLangkah+" langkah");
        return  jlhLangkah;
    }
    public void tidur(){
        int umur = umurKucing+1;
        System.out.println(umur);
    }
    public static void main(String args[]){
        KucingKesayangan kitty = new KucingKesayangan();
        kitty.setNamaKucing("Nero");
        kitty.setRasKucing("Kucing Anatolia");
        kitty.setUmurKucing(2);
        kitty.getNamaKucing();
        kitty.berjalan(10);
        kitty.mengeong();
        kitty.tidur();
    }
}
