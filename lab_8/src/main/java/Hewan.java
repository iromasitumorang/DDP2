public class Hewan implements MakhlukHidup {
    private int jumlahKaki;

    @Override
    public String makan() {
        return "hewan : hewan dan tumbuhan";
    }
    @Override
    public String bernafas() {
        return "hewan : oksigen";
    }
    @Override
    public String bergerak() {
        return "hewan : cepat";
    }

    public void setJumlahKaki(int jumlahKaki) {
        this.jumlahKaki = jumlahKaki;
    }

    public int getJumlahKaki() {
        return jumlahKaki;
    }

    public Hewan(int jumlahKaki) {
        this.jumlahKaki = jumlahKaki;
    }

    public Hewan() {
    }
}
