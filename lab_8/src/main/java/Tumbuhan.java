public class Tumbuhan implements MakhlukHidup {
    private boolean punyaBuah=false;

    @Override
    public String makan() {
        return "tumbuhan : nutrisi tanah";
    }

    @Override
    public String bernafas() {
        return "tumbuhan : karbondioksida";
    }

    @Override
    public String bergerak() {
        return "tumbuhan : diam";
    }

    public void setPunyaBuah(boolean punyaBuah) {
        this.punyaBuah = punyaBuah;
    }

    public boolean isPunyaBuah() {
        return punyaBuah;
    }

    public Tumbuhan(boolean punyaBuah) {
        this.punyaBuah = punyaBuah;
    }
    public Tumbuhan(){
    }
}
