public class Anak extends Manusia {
    private String nama, tmpLahir, derajatSekolah;
    private int usia, tb, bb;

    @Override
    public String makan() {
        return "anak : hewan";
    }

    @Override
    public String bernafas() {
        return "anak : oksigen";
    }

    @Override
    public String bergerak() {
        return "anak : cepat";
    }

    @Override
    public void setNama(String nama) {
        this.nama = nama;
    }

    @Override
    public String getNama() {
        return nama;
    }

    @Override
    public void setUsia(int usia) {
        this.usia = usia;
    }

    @Override
    public int getUsia() {
        return usia;
    }

    @Override
    public void setTmpLahir(String tmpLahir) {
        this.tmpLahir = tmpLahir;
    }

    @Override
    public String getTmpLahir() {
        return tmpLahir;
    }

    public void setDerajatSekolah(String derajatSekolah) {
        this.derajatSekolah = derajatSekolah;
	   System.out.println();
    }

    public String getDerajatSekolah() {
        return derajatSekolah;
    }

    @Override
    public void setTb(int tb) {
        this.tb = tb;
    }

    @Override
    public int getTb() {
        return tb;
    }

    @Override
    public void setBb(int bb) {
        this.bb = bb;
    }

    @Override
    public int getBb() {
        return bb;
    }

    public Anak(String nama, String tmpLahir, int usia, int tb, int bb,String derajatSekolah) {
        super(nama, tmpLahir, usia, tb, bb);
        this.nama = nama;
        this.tmpLahir = tmpLahir;
        this.usia = usia;
        this.tb = tb;
        this.bb = bb;
        this.derajatSekolah = derajatSekolah;
    }

    public Anak() {
        super();
    }
}
