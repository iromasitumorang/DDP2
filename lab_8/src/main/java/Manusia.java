public class Manusia implements MakhlukHidup  {
    private String nama;
    private String tmpLahir;
    private int usia, tb, bb;

    @Override
    public String makan() {
        return "manusia : hewan dan tumbuhan";
    }

    @Override
    public String bernafas() {
        return "manusia : oksigen";
    }

    @Override
    public String bergerak() {
        return "manusia : sedang";
    }

    public void setNama(String nama){
        this.nama = nama;
    }
    public String getNama(){
        return nama;
    }
    public void setTmpLahir(String tmpLahir){
        this.tmpLahir=tmpLahir;
    }
    public String getTmpLahir(){
        return tmpLahir;
    }
    public void setUsia(int usia){
        this.usia=usia;
    }
    public int getUsia(){
        return usia;
    }
    public void setTb(int tb){
        this.tb=tb;
    }
    public int getTb(){
        return tb;
    }
    public void setBb(int bb){
        this.bb=bb;
    }
    public int getBb(){
        return bb;
    }

    public Manusia(String nama, String tmpLahir, int usia, int tb, int bb) {
        this.nama = nama;
        this.tmpLahir = tmpLahir;
        this.usia = usia;
        this.tb = tb;
        this.bb = bb;
    }
    public Manusia(){

    }
}
