public class Herbivora extends Hewan {
    private int jumlahKaki;

    @Override
    public String makan() {
        return "herbivora : tumbuhan";
    }

    @Override
    public String bernafas() {
        return "herbivora : oksigen";
    }

    @Override
    public String bergerak() {
        return "herbivora : cepat";
    }

    @Override
    public void setJumlahKaki(int jumlahKaki) {
        this.jumlahKaki = jumlahKaki;
    }

    @Override
    public int getJumlahKaki() {
        return jumlahKaki;
    }

    public Herbivora(int jumlahKaki) {
        super(jumlahKaki);
        this.jumlahKaki = jumlahKaki;
    }

    public Herbivora() {
    }
}
