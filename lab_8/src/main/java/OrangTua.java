import java.util.ArrayList;

public class OrangTua extends Manusia{
    private String nama, tmpLahir,pekerjaan;
    private int usia, tb,bb;
    ArrayList<Anak> arrAnak= new ArrayList<Anak>();
    public OrangTua(String nama, String tmpLahir, int usia, int tb, int bb, String pekerjaan, Anak arrAnak) {
        super(nama, tmpLahir, usia, tb, bb);
        this.nama = nama;
        this.tmpLahir = tmpLahir;
        this.pekerjaan = pekerjaan;
        this.usia = usia;
        this.tb = tb;
        this.bb = bb;
        this.arrAnak.add(arrAnak);
    }

    public OrangTua() {
        super();
    }

    @Override
    public String makan() {
        return "orang tua : tumbuhan";
    }

    @Override
    public String bernafas() {
        return "orang tua : oksigen";
    }
    @Override
    public String bergerak() {
        return "orang tua  : lambat";
    }

    @Override
    public void setNama(String nama) {
        this.nama = nama;
    }

    @Override
    public String getNama() {
        return nama;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    @Override
    public void setTmpLahir(String tmpLahir) {
        this.tmpLahir = tmpLahir;
    }

    @Override
    public String getTmpLahir() {
        return tmpLahir;
    }

    @Override
    public void setUsia(int usia) {
        this.usia = usia;
    }

    @Override
    public int getUsia() {
        return usia;
    }

    @Override
    public void setBb(int bb) {
        this.bb = bb;
    }

    @Override
    public int getBb() {
        return bb;
    }

    @Override
    public void setTb(int tb) {
        this.tb = tb;
    }

    @Override
    public int getTb() {
        return tb;
    }

    public void setAnak(ArrayList<Anak> arrAnak) {
        this.arrAnak = arrAnak;
    }

    public ArrayList<Anak> getAnak() {
        return arrAnak;
    }


}
