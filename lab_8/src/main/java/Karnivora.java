public class Karnivora extends Hewan {
    private int jumlahKaki;

    @Override
    public String makan() {
        return "karnivora : hewan";
    }

    @Override
    public String bernafas() {
        return "karnivora : oksigen";
    }
    @Override
    public String bergerak() {
        return "karnivora : cepat";
    }
    @Override
    public void setJumlahKaki(int jumlahKaki) {
        this.jumlahKaki = jumlahKaki;
    }

    @Override
    public int getJumlahKaki() {
        return jumlahKaki;
    }

    public Karnivora(int jumlahKaki) {
        super(jumlahKaki);
        this.jumlahKaki = jumlahKaki;
    }

    public Karnivora() {
    }
}
