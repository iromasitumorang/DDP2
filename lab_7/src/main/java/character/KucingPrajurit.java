package character;

public class KucingPrajurit extends KucingRakyat {
    public KucingPrajurit (String name, int health, int power){
        super(name,health,power);
    }

    public void attack(Kucing kucing){
        kucing.setHealth(kucing.getHealth()-(this.getPower()*2));
    }
}
