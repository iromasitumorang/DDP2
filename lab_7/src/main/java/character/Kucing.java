package character;

public class Kucing {

    private String name;
    private int health;
    private int power;
    
    public Kucing(){
	this.health=health;
	this.name=name;
	this.power=power;	
	}    
public Kucing(String name, int health, int power){
        this.name=name;
        this.health=health;
        this.power=power;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name=name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health=health;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power=power;
    }

    public void attack(Kucing kucing) {
        if(this.health>0){
            kucing.health =kucing.health-this.getPower();
        }
    }

    public void sleep() {
        if(this.health>0){
            this.health = this.getHealth()+20;
        }
    }

    public String status(){
        return "Nama : "+this.getName()+"\n"+"Health: "+this.getHealth()+"\n"+"Power: "+this.getPower();
    }
}