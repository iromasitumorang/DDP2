package character;

public class KucingBangsawan extends character.Kucing {

    private String title;
    public KucingBangsawan(String name, int health, int power, String title){
        super(name, health, power);
        this.title=title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title=title;
    }

    public void execute(KucingRakyat kucing){
        kucing.setHealth(0);
    }

    public String status(){
        return "Nama : "+this.getName()+"\n" +
                "Health : "+this.getHealth()+"\n" +
                "Power : "+this.getPower()+"\n" +
                "Title : "+this.getTitle();
    }

}
